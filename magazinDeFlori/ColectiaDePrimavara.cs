﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace magazinDeFlori
{
    public partial class ColectiaDePrimavara : UserControl
    {
        string connectionString = @"Data Source=WIN-5EV5TFI59R9\SQLEXPRESS;Initial Catalog=MagazinFlori;Integrated Security=True";
        private int numberProm = 0;
        public ColectiaDePrimavara()
        {
            InitializeComponent();
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                sqlConnection.Open();
                SqlDataAdapter sqlData = new SqlDataAdapter("Select Count(*) From Produse where Produse.Categorie='Colectia de primavara'", sqlConnection);
                sqlData.Fill(data);
                numberProm = int.Parse(data.Rows[0][0].ToString());
                sqlConnection.Close();
            }
        }

        private void ColectiaDePrimavara_Load(object sender, EventArgs e)
        {
            int[] arrayId = new int[numberProm];
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                sqlConnection.Open();
                SqlDataAdapter sqlData = new SqlDataAdapter("Select * From Produse where Produse.Categorie='Colectia de primavara'", sqlConnection);
                sqlData.Fill(data);
                for (int i = 0; i < numberProm; i++)
                {
                    arrayId[i] = int.Parse(data.Rows[i][0].ToString());
                }
                sqlConnection.Close();
                int itemRind = 0;
                int cresteY = 45;
                for (int i = 0; i < numberProm; i++)
                {
                    Panel panel = new Panel
                    {
                        BackColor = Color.FromArgb(255, 204, 179),
                        TabIndex = i,
                        Size = new Size(250, 350)
                    };
                    Label labelName = new Label
                    {
                        Location = new Point(10, 10),
                        AutoSize = false,
                        Size = new Size(230, 30),
                        Name = "labelName",
                        TabIndex = i,
                        Text = data.Rows[i][1].ToString(),
                        Font = new Font("Arial", 15, FontStyle.Bold)
                    };
                    MemoryStream ms = new MemoryStream((byte[])data.Rows[i][5]);
                    PictureBox imagineProdus = new PictureBox
                    {
                        Location = new Point(10, 50),
                        Size = new Size(230, 190),
                        Image = Image.FromStream(ms),
                        SizeMode = PictureBoxSizeMode.StretchImage
                    };
                    Label descriere = new Label
                    {
                        Location = new Point(10, 245),
                        AutoSize = false,
                        Size = new Size(230, 60),
                        Name = "descriere",
                        TabIndex = i,
                        Text = data.Rows[i][3].ToString(),
                        Font = new Font("Arial", 12)
                    };
                    Label pret = new Label
                    {
                        Location = new Point(10, 310),
                        AutoSize = false,
                        Size = new Size(110, 30),
                        Name = "pret",
                        TabIndex = i,
                        Text = data.Rows[i][2].ToString() + " Lei",
                        Font = new Font("Arial", 14, FontStyle.Bold),
                        ForeColor = Color.FromArgb(245, 138, 8)
                    };
                    Button addProdus = new Button
                    {
                        Location = new Point(120, 310),
                        Size = new Size(110, 30),
                        Name = "addProdus",
                        TabIndex = i,
                        Text = "Comanda",
                        Font = new Font("Arial", 14),
                        BackColor = Color.FromArgb(245, 138, 8),
                        FlatStyle = FlatStyle.Flat
                    };
                    addProdus.FlatAppearance.BorderSize = 0;
                    if (itemRind != 2)
                    {
                        if (i % 2 == 0)
                        {
                            panel.Location = new Point(80, cresteY);
                            itemRind++;
                        }
                        else
                        {
                            panel.Location = new Point(400, cresteY);
                            itemRind++;
                        }
                    }
                    else
                    {
                        itemRind = 0;
                        cresteY += 400;
                        panel.Location = new Point(80, cresteY);
                    }
                    this.Controls.Add(panel);
                    panel.Controls.Add(labelName);
                    panel.Controls.Add(imagineProdus);
                    panel.Controls.Add(descriere);
                    panel.Controls.Add(pret);
                    panel.Controls.Add(addProdus);
                }
            }
        }
    }
}
