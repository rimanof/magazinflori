﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace magazinDeFlori
{
    public class Prodd
    {
        public int id { get; set; }
        public string nume { get; set; }
        public int pret { get; set; }
        public override string ToString()
        {
            return "ID: " + id + "Name: " + nume + " Pret: " + pret;
        }
        public Prodd(int ID, string NUME, int PRET)
        {
            id = ID;
            nume = NUME;
            pret = PRET;
        }
    }
}
