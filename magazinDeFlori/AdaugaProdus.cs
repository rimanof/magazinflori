﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace magazinDeFlori
{
    public partial class AdaugaProdus : UserControl
    {
        SqlConnection conn = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString);
        SqlCommand command;
        Produse produse1 = new Produse();
        string imgLoc = "";
        public AdaugaProdus()
        {
            InitializeComponent();
            comboBox1.SelectedItem = "Promotii";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Filter = "JPG Files (*.jpg)|*.jpg|JPEG Files (*.jpeg)|*.jpeg|All Files (*.*)|*.*";
                dlg.Title = "Select Employee Picture";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    imgLoc = dlg.FileName.ToString();
                    pictureBox1.ImageLocation = imgLoc;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Produse produse = new Produse();
            if ((textBox1.Text != "") && (textBox2.Text != "") && (textBox3.Text != ""))
            {
                bool isNumeric = textBox2.Text.All(char.IsDigit);
                if (isNumeric == true)
                {
                    try
                    {
                        if ((comboBox1.SelectedItem.ToString() == "Promotii") || (comboBox1.SelectedItem.ToString() == "Trandafiri criogenati") || (comboBox1.SelectedItem.ToString() == "Colectia de primavara") || (comboBox1.SelectedItem.ToString() == "Flori onomastica") || (comboBox1.SelectedItem.ToString() == "Ocazii speciale"))
                        {
                            if (Titlu.Text.ToString() == "Adaugare Produs Nou")
                            {
                                byte[] img = null;
                                FileStream fs = new FileStream(imgLoc, FileMode.Open, FileAccess.Read);
                                BinaryReader br = new BinaryReader(fs);
                                img = br.ReadBytes((int)fs.Length);
                                string sql = "Insert into Produse(Nume_Produs,Pret,Descriere,Categorie,Imagine) Values('" + textBox1.Text + "'," + textBox2.Text + ",'" + textBox3.Text + "','" + comboBox1.SelectedItem.ToString() + "',@img)";
                                if (conn.State != ConnectionState.Open)
                                {
                                    conn.Open();
                                }
                                command = new SqlCommand(sql, conn);
                                command.Parameters.Add(new SqlParameter("@img", img));
                                int x = command.ExecuteNonQuery();
                                imgLoc = "";
                                MessageBox.Show("Inregistrare efectuata cu succes!");
                                Form1.Instance.PnlContainer.Controls["produse1"].BringToFront();
                                conn.Close();
                            }
                            else if(Titlu.Text.ToString() == "Modifica Produs Existent")
                            {
                                if (conn.State != ConnectionState.Open)
                                {
                                    conn.Open();
                                }
                                if (imgLoc != "")
                                {
                                    FileStream fs = new FileStream(imgLoc, FileMode.Open, FileAccess.Read);
                                    BinaryReader br = new BinaryReader(fs);
                                    byte[] img = br.ReadBytes((int)fs.Length);
                                    string sql = "Update Produse SET Nume_Produs='" + textBox1.Text + "',Pret=" + textBox2.Text + ",Descriere='" + textBox3.Text + "',Categorie='" + comboBox1.SelectedItem.ToString() + "',Imagine=@img Where Id_Produs=" + textBox1.TabIndex + "";
                                    command = new SqlCommand(sql, conn);
                                    command.Parameters.Add(new SqlParameter("@img", img));
                                    command.ExecuteNonQuery();
                                }
                                else
                                {
                                    string sql = "Update Produse SET Nume_Produs='" + textBox1.Text + "',Pret=" + textBox2.Text + ",Descriere='" + textBox3.Text + "',Categorie='" + comboBox1.SelectedItem.ToString() + "' Where Id_Produs=" + textBox1.TabIndex + "";
                                    command = new SqlCommand(sql, conn);
                                    command.ExecuteNonQuery();
                                }
                                conn.Close();
                                MessageBox.Show("Produs Modificat cu succes!");
                                Form1.Instance.PnlContainer.Controls["produse1"].BringToFront();
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Pretul trebue sa fie de tip numeric!");
                }
            }
            else
            {
                MessageBox.Show("Completati toate spatiile libere!");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form1.Instance.PnlContainer.Controls["produse1"].BringToFront();
        }
    }
}
