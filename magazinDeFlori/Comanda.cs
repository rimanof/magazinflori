﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magazinDeFlori
{
    class Comanda
    {
        public List<int> id_Produs { get; set; }
        public int id_Comanda { get; set; }
        public string Client { get; set; }
        public string Telefon { get; set; }
        public string Mail { get; set; }
        public string Statut { get; set; }
        public string Date { get; set; }
        public string Adresa { get; set; }
        public List<int> Cantitate { get; set; }
        public int Total { get; set; }
    }
}
