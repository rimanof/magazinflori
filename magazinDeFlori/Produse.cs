﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace magazinDeFlori
{
    public partial class Produse : UserControl
    {
        SqlConnection conn = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString);
        SqlCommand command;
        private int id = 0;
        public Produse()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //if(!Form1.Instance.PnlContainer.Controls.ContainsKey("adaugaProdus1"))
            //{
            //    AdaugaProdus ap = new AdaugaProdus();
            //    ap.Dock = DockStyle.Fill;
            //    Form1.Instance.PnlContainer.Controls.Add(ap);
            //}
            Form1.Instance.PnlContainer.Controls["adaugaProdus1"].BringToFront();
            Form1.Instance.PnlContainer.Controls["adaugaProdus1"].Controls["Titlu"].Text = "Adaugare Produs Nou";
            Form1.Instance.PnlContainer.Controls["adaugaProdus1"].Controls["textBox1"].Text = "";
            Form1.Instance.PnlContainer.Controls["adaugaProdus1"].Controls["textBox2"].Text = "";
            Form1.Instance.PnlContainer.Controls["adaugaProdus1"].Controls["textBox3"].Text = "";
            Form1.Instance.PnlContainer.Controls["adaugaProdus1"].Controls["comboBox1"].Text = "Promotii";

        }

        private void Produse_Load(object sender, EventArgs e)
        {
            AfisareProdd();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                Form1.Instance.PnlContainer.Controls["adaugaProdus1"].BringToFront();
                Form1.Instance.PnlContainer.Controls["adaugaProdus1"].Controls["Titlu"].Text = "Modifica Produs Existent";
                int index = dataGridView1.SelectedRows[0].Index;
                bool converted = Int32.TryParse(dataGridView1[0, index].Value.ToString(), out id);
                if (converted == false)
                    return;

                Flowers floare = new Flowers();
                using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
                {
                    DataTable data = new DataTable();
                    data.Clear();
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }
                    SqlDataAdapter sqlData = new SqlDataAdapter("Select * From Produse where Produse.Id_Produs=" + id + "", sqlConnection);
                    sqlData.Fill(data);
                    floare.id = int.Parse(data.Rows[0][0].ToString());
                    floare.nume= data.Rows[0][1].ToString();
                    floare.pret=int.Parse(data.Rows[0][2].ToString());
                    floare.descriere= data.Rows[0][3].ToString();
                    floare.categorie=data.Rows[0][5].ToString();
                    sqlConnection.Close();
                }
                Form1.Instance.PnlContainer.Controls["adaugaProdus1"].Controls["textBox1"].Text = floare.nume;
                Form1.Instance.PnlContainer.Controls["adaugaProdus1"].Controls["textBox1"].TabIndex = floare.id;
                Form1.Instance.PnlContainer.Controls["adaugaProdus1"].Controls["textBox2"].Text = floare.pret.ToString();
                Form1.Instance.PnlContainer.Controls["adaugaProdus1"].Controls["textBox3"].Text = floare.descriere;
                Form1.Instance.PnlContainer.Controls["adaugaProdus1"].Controls["comboBox1"].Text = floare.categorie.ToString();
            }
            else
            {
                MessageBox.Show("Selectati randul pe care doriti sa-l modificati!");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int index = dataGridView1.SelectedRows[0].Index;
                bool converted = Int32.TryParse(dataGridView1[0, index].Value.ToString(), out id);
                if (converted == false)
                    return;
                DialogResult dialogResult = MessageBox.Show("Sunteti sigur?", "Some Title", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    if (id != 1)
                    {
                        if (conn.State != ConnectionState.Open)
                        {
                            conn.Open();
                        }
                        string sql = "alter table Produse nocheck constraint all BEGIN TRANSACTION DELETE FROM Produse WHERE Id_Produs = " + id + " COMMIT alter table Produse check constraint all";
                        command = new SqlCommand(sql, conn);
                        command.ExecuteNonQuery();
                        conn.Close();
                        MessageBox.Show("Produs sters cu succes");
                        AfisareProdd();
                    }
                    else
                    {
                        MessageBox.Show("Acest produs nu poate fi sters la moment!");
                    }
                }
                else if (dialogResult == DialogResult.No)
                {

                }
            }
            else
            {
                MessageBox.Show("Selectati randul pe care doriti sa-l stergeti!");
            }
        }
        private void AfisareProdd()
        {
            using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SqlDataAdapter sqlData = new SqlDataAdapter("SELECT * From Produse", sqlConnection);
                sqlData.Fill(data);
                dataGridView1.DataSource = data;
                sqlConnection.Close();
            }
        }
    }
}
