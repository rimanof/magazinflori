﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Linq.Expressions;
using System.Numerics;
using System.Runtime.InteropServices;

namespace magazinDeFlori
{
    public partial class ListaComenzi : UserControl
    {
        
        private int numberCom = 0;
        private int numberEll = 0;
        public List<int> cantity = new List<int>();
        private List<int> id_Prod = new List<int>();
        private List<int> Price = new List<int>();
        public int numberClick = 0;
        public string Statut = "Activ";
        public ListaComenzi()
        {
            InitializeComponent();
            using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SqlDataAdapter sqlData = new SqlDataAdapter("Select Count(*) From Produse", sqlConnection);
                sqlData.Fill(data);
                numberEll = int.Parse(data.Rows[0][0].ToString());
                sqlConnection.Close();
            }
        }
        public void AfisareElemLista()
        {
            int cresteY = 76;
            using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SqlDataAdapter sqlData = new SqlDataAdapter("Select Count(*) From ListaComenzi", sqlConnection);
                sqlData.Fill(data);
                numberCom = int.Parse(data.Rows[0][0].ToString());
                sqlConnection.Close();
            }
            panelContent.Controls.Clear();
            {
                Label Titlu = new Label
                {
                    Location = new Point(266, 2),
                    AutoSize = true,
                    Name = "Titlu",
                    TabIndex = 0,
                    Text = "Comanda tA:",
                    Font = new Font("Arial", 20, FontStyle.Bold),
                    ForeColor = Color.FromArgb(245, 138, 8)
                };
                Label Nume_Produs = new Label
                {
                    Location = new Point(37, 42),
                    AutoSize = true,
                    Name = "Nume_Produs",
                    TabIndex = 1,
                    Text = "Nume Produs:",
                    Font = new Font("Arial", 20),
                    ForeColor = Color.Black
                };
                Label Pret = new Label
                {
                    Location = new Point(226, 42),
                    AutoSize = true,
                    Name = "Pret (buc)",
                    TabIndex = 2,
                    Text = "Pret(buc)",
                    Font = new Font("Arial", 20),
                    ForeColor = Color.Black
                };
                Label Cantitate = new Label
                {
                    Location = new Point(371, 42),
                    AutoSize = true,
                    Name = "Cantitate",
                    TabIndex = 2,
                    Text = "Cantitate",
                    Font = new Font("Arial", 20),
                    ForeColor = Color.Black
                };
                Label Total = new Label
                {
                    Location = new Point(505, 42),
                    AutoSize = true,
                    Name = "Total",
                    TabIndex = 2,
                    Text = "Total",
                    Font = new Font("Arial", 20),
                    ForeColor = Color.Black
                };
                panelContent.Controls.Add(Titlu);
                panelContent.Controls.Add(Nume_Produs);
                panelContent.Controls.Add(Pret);
                panelContent.Controls.Add(Cantitate);
                panelContent.Controls.Add(Total);
            }
            id_Prod.Clear();
            using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SqlDataAdapter sqlData = new SqlDataAdapter("Select * From ListaComenzi", sqlConnection);
                sqlData.Fill(data);
                sqlConnection.Close();

                for (int i = 0; i < numberCom; i++)
                {
                    id_Prod.Add(int.Parse(data.Rows[i][0].ToString()));
                    if (cantity.Count() < id_Prod.Count())
                    {
                        cantity.Insert(0, 1);
                    }
                }

                for (int i = 0; i < numberCom; i++)
                {
                    Panel panel = new Panel
                    {
                        TabIndex = i,
                        Size = new Size(645, 31),
                        Location = new Point(40, cresteY)
                    };
                    cresteY += 37;
                    Label label = new Label
                    {
                        Location = new Point(0, 3),
                        AutoSize = false,
                        Size = new Size(190, 31),
                        TabIndex = i,
                        Text = data.Rows[i][1].ToString(),
                        Font = new Font("Arial", 15),
                        ForeColor = Color.Black
                    };
                    Label label1 = new Label
                    {
                        Location = new Point(200, 3),
                        AutoSize = false,
                        Size = new Size(100, 31),
                        TabIndex = i,
                        Text = data.Rows[i][2].ToString() + " Lei",
                        Font = new Font("Arial", 15),
                        ForeColor = Color.Black
                    };
                    Button button = new Button
                    {
                        Location = new Point(345, 0),
                        AutoSize = false,
                        Size = new Size(30, 30),
                        Name = i.ToString(),
                        TabIndex = i,
                        Text = "-",
                        ForeColor = Color.Black
                    };
                    button.Click += new EventHandler(this.MyButtonHandler);
                    Label label2 = new Label
                    {
                        Location = new Point(385, 3),
                        AutoSize = false,
                        Size = new Size(30, 30),
                        TabIndex = i,
                        Text = cantity[i].ToString(),
                        Font = new Font("Arial", 15),
                        ForeColor = Color.Black
                    };
                    Button button1 = new Button
                    {
                        Location = new Point(415, 0),
                        AutoSize = false,
                        Size = new Size(30, 30),
                        TabIndex = i,
                        Text = "+",
                        Name = i.ToString(),
                        ForeColor = Color.Black
                    };
                    button1.Click += new EventHandler(this.MyButtonHandler1);
                    Label label3 = new Label
                    {
                        Location = new Point(473, 3),
                        AutoSize = false,
                        Size = new Size(60, 31),
                        TabIndex = i,
                        Text = (cantity[i] * int.Parse(data.Rows[i][2].ToString())).ToString(),
                        Font = new Font("Arial", 15),
                        ForeColor = Color.Black
                    };
                    Button button2 = new Button
                    {
                        Location = new Point(540, 0),
                        AutoSize = false,
                        Size = new Size(90, 30),
                        TabIndex = i,
                        Text = "Delete",
                        Name = data.Rows[i][0].ToString(),
                        ForeColor = Color.Black
                    };
                    button2.Click += new EventHandler(this.MyButtonHandler2);

                    panelContent.Controls.Add(panel);
                    panel.Controls.Add(label);
                    panel.Controls.Add(label1);
                    panel.Controls.Add(button);
                    panel.Controls.Add(label2);
                    panel.Controls.Add(button1);
                    panel.Controls.Add(label3);
                    panel.Controls.Add(button2);

                    panelContent.Focus();
                }
                //for (int i = 0; i < id_Prod.Count(); i++)
                //{
                //    MessageBox.Show(id_Prod[i].ToString()+" cantitate: "+cantity[i].ToString());
                //}
                //MessageBox.Show("Nr ell: "+cantity.Count().ToString());
            }
        }
        private void MyButtonHandler(object sender, EventArgs e)
        {
            if (cantity[(sender as Button).TabIndex] != 1)
            {
                cantity[(sender as Button).TabIndex]--;
                AfisareElemLista();
            }
        }
        private void MyButtonHandler1(object sender, EventArgs e)
        {
            if (cantity[(sender as Button).TabIndex] != 5)
            {
                cantity[(sender as Button).TabIndex]++;
                AfisareElemLista();
            }
        }
        private void MyButtonHandler2(object sender, EventArgs e)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SqlCommand sqlCommand;
                sqlCommand = new SqlCommand("Delete From ListaComenzi Where ListaComenzi.Id_Prod=" + (sender as Button).Name + "", sqlConnection);
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            cantity.RemoveAt((sender as Button).TabIndex);
            AfisareElemLista();
        }

        private void BTN2_Click(object sender, EventArgs e)
        {
            if (numberClick == 0)
            {
                if (numberCom != 0)
                {
                    panelContent.Controls.Clear();
                    BTN1.Show();
                    BTN2.Text = "Comanda";
                    label7.Show();
                    numberClick = 1;
                    panelContent.Controls.Add(label1);
                    panelContent.Controls.Add(label2);
                    panelContent.Controls.Add(label5);
                    panelContent.Controls.Add(label3);
                    panelContent.Controls.Add(label4);
                    panelContent.Controls.Add(label6);
                    panelContent.Controls.Add(label8);
                    panelContent.Controls.Add(textBox1);
                    panelContent.Controls.Add(textBox2);
                    panelContent.Controls.Add(textBox3);
                    panelContent.Controls.Add(textBox4);
                    panelContent.Controls.Add(dateTimePicker1);
                }
                else
                {
                    MessageBox.Show("Alegeti Produse!");
                }
            }
            else if (numberClick == 1)
            {

                    if ((textBox1.Text != "") && (textBox2.Text != "") && (textBox3.Text != ""))
                    {
                        bool isNumeric = textBox2.Text.All(char.IsDigit);
                        int nrEll = 0;
                        if (isNumeric == true)
                        {
                            Int64 n = Int64.Parse(textBox2.Text);
                            while (n != 0)
                            {
                                nrEll++;
                                n = n / 10;
                            }
                        }
                        if ((isNumeric == true) && ((nrEll == 8) || (nrEll == 11)))
                        {
                            using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
                            {
                                if (conn.State != ConnectionState.Open)
                                {
                                    conn.Open();
                                }
                                SqlCommand sqlCommand;
                                String sql = "Insert into Comenzi(Nume_Client,Telefon,Mail,Statut,Adresa,Data) Values('" + textBox1.Text + "','" + textBox2.Text.ToString() + "','" + textBox4.Text + "','" + Statut + "','" + textBox3.Text + "','" + dateTimePicker1.Value.Date.ToString("yyyy-MM-dd") + "')";
                                sqlCommand = new SqlCommand(sql, conn);
                                sqlCommand.ExecuteNonQuery();
                                DataTable data = new DataTable();
                                DataTable data1 = new DataTable();
                                data.Clear();
                                SqlDataAdapter sqlData1 = new SqlDataAdapter("SELECT * FROM Produse;", conn);
                                sqlData1.Fill(data1);
                                for (int i = 0; i < id_Prod.Count(); i++)
                                {
                                    for (int j = 0; j < numberEll; j++)
                                    {
                                        if (id_Prod[i] == int.Parse(data1.Rows[j][0].ToString()))
                                        {
                                            int a = int.Parse(data1.Rows[i][2].ToString()) * cantity[i];
                                        Price.Add(a);
                                        }
                                    }
                                }
                                SqlDataAdapter sqlData = new SqlDataAdapter("SELECT MAX(Id_Comanda) FROM Comenzi", conn);
                                sqlData.Fill(data);
                                for (int i = 0; i < id_Prod.Count(); i++)
                                {
                                    sql = "Insert into Connections(Id_Prodd,Id_Comm,Cantitate,Price) Values(" + id_Prod[i] + "," + int.Parse(data.Rows[0][0].ToString()) + "," + cantity[i] + "," + Price[i] + ")";
                                    sqlCommand = new SqlCommand(sql, conn);
                                    sqlCommand.ExecuteNonQuery();
                                }
                                Price.Clear();
                                sql = "Delete From ListaComenzi";
                                sqlCommand = new SqlCommand(sql, conn);
                                sqlCommand.ExecuteNonQuery();
                                conn.Close();
                                MessageBox.Show("Comanda Dumneavoastra a fost Inregistrata");
                                AfisareElemLista();
                                BTN1.Hide();
                                label7.Hide();
                                BTN2.Text = "Continua";
                            }
                        }
                        else
                        {
                            MessageBox.Show("Introduceti un numar de telefon valid");
                        }
                    }
                    else MessageBox.Show("Completati toate datele marcate cu *");

            }
        }

        public void BTN1_Click(object sender, EventArgs e)
        {
            AfisareElemLista();
            BTN1.Hide();
            label7.Hide();
            numberClick = 0;
            BTN2.Text = "Continua";
        }
    }
}
