﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace magazinDeFlori
{
    public partial class Form1 : Form
    {
        public int numberMenu = 0;
        public FormLOGIN fL = new FormLOGIN();
        public Form1()
        {
            InitializeComponent();
            MoveSidePanel(button1);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MoveSidePanel(button1);
            userControl(home1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MoveSidePanel(button2);

            if (numberMenu == 0)
            {
                userControl11.AfisareElemente("Promotii");
                userControl(userControl11);
                userControl11.Focus();
            }
            else if (numberMenu == 1)
            {
                userControl(produse1);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MoveSidePanel(button3);

            if (numberMenu == 0)
            {
                userControl11.AfisareElemente("Ocazii speciale");
                userControl(userControl11);
                userControl11.Focus();
            }
            else if (numberMenu == 1)
            {
                userControl(comenzi1);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MoveSidePanel(button4);
            userControl11.AfisareElemente("Trandafiri criogenati");
            userControl(userControl11);
            userControl11.Focus();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MoveSidePanel(button5);
            userControl11.AfisareElemente("Colectia de primavara");
            userControl(userControl11);
            userControl11.Focus();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            MoveSidePanel(button6);
            userControl11.AfisareElemente("Flori onomastica");
            userControl(userControl11);
            userControl11.Focus();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            MoveSidePanel(button8);
            userControl(listaComenzi1);
            listaComenzi1.Focus();
            listaComenzi1.AfisareElemLista();
            listaComenzi1.BTN1.Hide();
            listaComenzi1.label7.Hide();
            listaComenzi1.numberClick = 0;
            listaComenzi1.BTN2.Text = "Continua";
            //listaComenzi1.cantity.Clear();
        }
        private void button9_Click(object sender, EventArgs e)
        {
            if (numberMenu == 0)
            {
                this.Hide();
                fL.Show();
            }
            else
            {
                if (numberMenu == 1)
                {
                    DialogResult dialogResult = MessageBox.Show("Sunteti sigur ca doriti sa iesit din regim Administrator??", "Some Title", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        AfisareMeniuDeBaza();
                    }
                }
            }
        }
        public void ModificaMeniu()
        {
            button2.Text = "Produse";
            button2.Image = magazinDeFlori.Properties.Resources.icons8_system_report_3;
            button3.Text = "Comenzi";
            button3.Image = magazinDeFlori.Properties.Resources.icons8_system_task_1;
            button4.Hide();
            button5.Hide();
            button6.Hide();
            button8.Hide();
            MoveSidePanel(button1);
            numberMenu = 1;
            label3.Visible = true;
            this.Show();
            userControl(home1);
        }
        public void AfisareMeniuDeBaza()
        {
            label3.Visible = false;
            numberMenu = 0;
            button2.Text = "Promotii";
            button2.Image = magazinDeFlori.Properties.Resources.icons8_discount;
            button3.Text = "Ocazii speciale";
            button3.Image = magazinDeFlori.Properties.Resources.icons8_flower_bouquet;
            button4.Show();
            button5.Show();
            button6.Show();
            button8.Show();
        }
        private void MoveSidePanel(Button button)
        {
            SidePanel.Height = button.Height;
            SidePanel.Top = button.Top;
        }


        static Form1 _obj;
        public static Form1 Instance
        {
            get
            {
                if (_obj == null)
                {
                    _obj = new Form1();
                }
                return _obj;
            }
        }
        public Panel PnlContainer
        {
            get { return panelContainer; }
            set { panelContainer = value; }
        }

        private void userControl(UserControl form)
        {
            form.BringToFront();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _obj = this;
            //Produse form = new Produse();
            //form.Dock = DockStyle.Fill;
            //panelContainer.Controls.Add(form);
            userControl(home1);
        }
        int mov;
        int movX;
        int movY;
        private void button10_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mov = 1;
            movX = e.X;
            movY = e.Y;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mov == 1)
            {
                this.SetDesktopLocation(MousePosition.X - movX, MousePosition.Y - movY);
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mov = 0;
        }

        private void panel2_MouseDown_1(object sender, MouseEventArgs e)
        {
            mov = 1;
            movX = e.X;
            movY = e.Y;
        }

        private void panel2_MouseMove_1(object sender, MouseEventArgs e)
        {
            if (mov == 1)
            {
                this.SetDesktopLocation(MousePosition.X - movX - 244, MousePosition.Y - movY);
            }
        }

        private void panel2_MouseUp_1(object sender, MouseEventArgs e)
        {
            mov = 0;
        }
    }
}
