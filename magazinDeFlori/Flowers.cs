﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magazinDeFlori
{
    class Flowers
    {
        public int id { get; set; }
        public string nume { get; set; }
        public int pret { get; set; }
        public string descriere { get; set; }
        public string categorie { get; set; }
    }
}
