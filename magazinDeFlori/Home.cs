﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace magazinDeFlori
{
    public partial class Home : UserControl
    {
        SqlConnection conn = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString);
        public Home()
        {
            InitializeComponent();
        }

        private void Home_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int id = 0;
                string name = null;
                int price = 0;
                using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
                {
                    DataTable data = new DataTable();
                    data.Clear();
                    if (sqlConnection.State != ConnectionState.Open)
                    {
                        sqlConnection.Open();
                    }
                    SqlDataAdapter sqlData = new SqlDataAdapter("Select Produse.Id_Produs, Produse.Nume_Produs, Produse.Pret From Produse Where Produse.Nume_Produs='Cucereste-o'", sqlConnection);
                    sqlData.Fill(data);
                    id = int.Parse(data.Rows[0][0].ToString());
                    name = data.Rows[0][1].ToString();
                    price = int.Parse(data.Rows[0][2].ToString());
                    sqlConnection.Close();
                }
                if (ProdusInexist(id) != 1)
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }
                    SqlCommand sqlCommand;
                    String sql = "Insert into ListaComenzi(Id_Prod,Nume_Prod,Pret_Prod) Values(" + id + ",'" + name + "'," + price + ")";
                    sqlCommand = new SqlCommand(sql, conn);
                    sqlCommand.ExecuteNonQuery();

                    conn.Close();
                    MessageBox.Show("Comanda dumneavoastra a fost inregistrata!");
                }
                else
                {
                    MessageBox.Show("Produsul a fost deja adaugat!");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private int ProdusInexist(int ID)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Properties.Settings.Default.MagazinFloriConnectionString))
            {
                DataTable data = new DataTable();
                data.Clear();
                if (sqlConnection.State != ConnectionState.Open)
                {
                    sqlConnection.Open();
                }
                SqlDataAdapter sqlData = new SqlDataAdapter("Select Count(*) From ListaComenzi Where Id_Prod=" + ID + "", sqlConnection);
                sqlData.Fill(data);
                sqlConnection.Close();
                return int.Parse(data.Rows[0][0].ToString());
            }
        }
    }
}
